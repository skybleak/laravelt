<?php

namespace App\Http\Middleware;

use Closure;

class APIParticipant {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if ($request->key !== config('constants.api_key')['participant']) {
            return response()->json(['success' => false, 'msg' => 'key is not authorized'], 403);
        }

        return $next($request);
    }
}