**Установка**
1. composer install
2. docker-compose up -d

**Api route**

1. GET - /participant/{events} => method - show
params:
    * id_event: {id_event}
2. POST - /participant/ => method - store   
params:
    * name: {name}
    * surname: {surname}
    * email: {email}
    * id_event: {id_event}
    * key: {key}  
    
3. UPDATE - /participant/{participant} => method - update
params:
    * id_participant: {id_participant}
    * name: {name}
    * surname: {surname}
    * email: {email}
    * id_event: {id_event}
    * key: {key}  
4. DELETE - /participant/{id} => method - delete
params:
    * id_participant: {id_participant}

**Ключь для доступа к API**

key:wgAQcAXOfVcL2AJZhYHMeYXHP82aMwhtIXL9ITYyUOqrY2AaE7GOIptXiCLgV8HE