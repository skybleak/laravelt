<?php


namespace App\Helpers\Contracts;

use App\Events;
use App\Http\Requests\StoreParticipantPost;
use App\Http\Requests\UpdateParticipantPut;
use App\Participant;

interface ParticipantInterface
{
    public function show(Events $events);
    public function store(StoreParticipantPost $request);
    public function update(UpdateParticipantPut $request, Participant $participant);
    public function delete(int $id);
}